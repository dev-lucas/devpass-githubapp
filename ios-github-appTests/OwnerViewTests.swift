//
//  OwnerViewTests.swift
//  ios-github-appTests
//
//  Created by Lucas Gomes on 24/04/23.
//

import XCTest
import SnapshotTesting
@testable import ios_github_app

final class OwnerViewTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testOwnerView() throws {
        let view = OwnerView()
        view.updateView(with: .init(title: "Owner",
                                    owner: "Lucas Gomes",
                                    bio: "IOS Developer",
                                    image: UIImage(systemName: "heart.fill") ?? UIImage(),
                                    html_url: "-"))
        
        assertSnapshot(matching: view, as: .image(size: .init(width: 375, height: 204)))
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
