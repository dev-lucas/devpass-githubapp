//
//  LicenseViewTests.swift
//  ios-github-appTests
//
//  Created by Lucas Gomes on 24/04/23.
//

import XCTest
import SnapshotTesting
@testable import ios_github_app

final class LicenseViewTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLicenseView() throws {
        let view = LicenseView()
        view.updateView(with: .init(title: "License",
                                    name: "GNU General Public License v2.0",
                                    code: "GPL-2.0",
                                    url: "-"))
        
        assertSnapshot(matching: view, as: .image(size: .init(width: 375, height: 214)))
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
