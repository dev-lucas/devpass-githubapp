//
//  RepositoryInfoViewTests.swift
//  ios-github-appTests
//
//  Created by Lucas Gomes on 24/04/23.
//

import XCTest
import SnapshotTesting
@testable import ios_github_app

final class RepositoryInfoViewTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRepositoryInfoView() throws {
        let view = RepositoryInfoView()
        view.updateView(with: .init(name: "hereminders-ios",
                                    description: "O Hereminders é um app de lembretes geolocalizados, implementado em Swift com arquitetura MVVM-C.",
                                    stars: 5,
                                    bifurcations: 17))
        
        assertSnapshot(matching: view, as: .image(size: .init(width: 375, height: 165.5)))
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
