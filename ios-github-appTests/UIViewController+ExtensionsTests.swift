//
//  UIViewController+ExtensionsTests.swift
//  ios-github-appTests
//
//  Created by Lucas Gomes on 18/04/23.
//

import XCTest
@testable import ios_github_app

final class UIViewController_ExtensionsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInsideNavigationController() throws {
        let viewController = SettingsViewController()
        let navigationController = viewController.insideNavigationController()
        
        XCTAssertEqual(viewController.navigationController, navigationController)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
