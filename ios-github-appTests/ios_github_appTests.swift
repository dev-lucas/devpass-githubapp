//
//  ios_github_appTests.swift
//  ios-github-appTests
//
//  Created by Lucas Gomes on 12/04/23.
//

import XCTest
import SnapshotTesting
@testable import ios_github_app

final class ios_github_appTests: XCTestCase {

    func testExample() throws {
        let view = UIView()
        let label = UILabel()

        label.text = "SnapshotTesting"
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = UIColor.green
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = .white
        view.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        assertSnapshot(matching: view, as: .image(size: .init(width: 300, height: 300)))
    }

}
