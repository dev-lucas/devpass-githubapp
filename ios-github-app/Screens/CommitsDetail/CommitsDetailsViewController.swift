//
//  CommitsDetailsViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 02/05/23.
//

import UIKit

final class CommitsDetailsViewController: UIViewController {
    
    private var commits: [CommitsModel]?
    
    private let commitsDetailsView: CommitsDetailsView = {
        let view = CommitsDetailsView()
        return view
    }()
    
    override func viewDidLoad() {
        setupNavigationBar()
        setupTableView()
    }
    
    override func loadView() {
        self.view = commitsDetailsView
    }
    
    static func instance(_ commits: [CommitsModel]) -> CommitsDetailsViewController {
        let controller: CommitsDetailsViewController = CommitsDetailsViewController(nibName: String(describing: self), bundle: nil)
        controller.commits = commits
        return controller
    }
    
    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Commits"
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        commitsDetailsView.tableView.dataSource = self
        commitsDetailsView.tableView.delegate = self
    }
    
    private func settingCommitAuthor(username: String?, name: String?) -> String {
        if let username = username {
            return String.ownerLogin(login: username)
        }
        
        if let name = name {
            return name
        }
        
        return "-"
    }
}

extension CommitsDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        commits?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommitsDetailsCellView.classIdentifier()) as? CommitsDetailsCellView else { fatalError("Generate CommitsDetailsCellView error") }
        guard let commit = commits?[indexPath.row] else { fatalError("Commit Cell info error") }
        cell.separatorInset.left = 16
        cell.selectionStyle = .none

        cell.updateView(with: CommitsDetailsCellViewConfiguration(message: commit.commit?.message ?? "-",
                                                                  author: settingCommitAuthor(username: commit.author?.login, name: commit.commit?.author.name)))
        return cell
    }
}

extension CommitsDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 90 : 70
    }
}
