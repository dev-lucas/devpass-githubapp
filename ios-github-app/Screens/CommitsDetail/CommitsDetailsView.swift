//
//  CommitsDetailsView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 02/05/23.
//

import UIKit

class CommitsDetailsView: UIView {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView = UIView()
        tableView.backgroundColor = ColorModel.background
        tableView.register(CommitsDetailsCellView.self, forCellReuseIdentifier: CommitsDetailsCellView.classIdentifier())
        return tableView
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension CommitsDetailsView {

    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(tableView)
    }
    
    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
