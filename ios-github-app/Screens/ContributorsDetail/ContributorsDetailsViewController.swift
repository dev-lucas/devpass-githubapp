//
//  ContributorsDetailsViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 01/05/23.
//

import UIKit

final class ContributorsDetailsViewController: UIViewController {
    
    private var contributors: [OwnerModel]?
    private let service = Service()
    
    private let contributorsDetailsView: ContributorsDetailsView = {
        let view = ContributorsDetailsView()
        return view
    }()
    
    override func viewDidLoad() {
        setupNavigationBar()
        setupTableView()
    }
    
    override func loadView() {
        self.view = contributorsDetailsView
    }
    
    static func instance(_ contributors: [OwnerModel]) -> ContributorsDetailsViewController {
        let controller: ContributorsDetailsViewController = ContributorsDetailsViewController(nibName: String(describing: self), bundle: nil)
        controller.contributors = contributors
        return controller
    }
    
    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Contributors"
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        contributorsDetailsView.tableView.dataSource = self
        contributorsDetailsView.tableView.delegate = self
    }
    
    private func setImageByUrl(_ image: UIImageView?, _ url: String?) {
        guard let url else { return }
        service.get(for: url) { (result: Result<UIImage, Error>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let success):
                    image?.image = success
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
            
        }
    }
    
    private func getOwnerInfoByUrl(_ url: String?, completion: @escaping (_ response: OwnerModel?) -> Void) {
        guard let url else { return }
        service.get(for: url) { (result: Result<OwnerModel, Error>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let success):
                    completion(success)
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
    }
}

extension ContributorsDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributors?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ContributorsDetailsCellView.classIdentifier()) as? ContributorsDetailsCellView else { fatalError("Generate SettingsViewCell error") }
        guard let contributor = contributors?[indexPath.row] else { fatalError("Language Cell info error") }
        cell.separatorInset.left = 16
        cell.selectionStyle = .none
        
        getOwnerInfoByUrl(contributor.url) { [weak self] response in
            cell.updateView(with: ContributorsDetailsCellViewConfiguration(name: response?.name ?? "-", username: response?.login ?? "-"))
            self?.setImageByUrl(cell.userImage, contributor.avatar_url)
        }

        return cell
    }
}

extension ContributorsDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
