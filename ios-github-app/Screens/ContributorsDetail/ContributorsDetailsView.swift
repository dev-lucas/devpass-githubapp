//
//  ContributorsDetailsView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 01/05/23.
//

import UIKit

class ContributorsDetailsView: UIView {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView = UIView()
        tableView.backgroundColor = ColorModel.background
        tableView.register(ContributorsDetailsCellView.self, forCellReuseIdentifier: ContributorsDetailsCellView.classIdentifier())
        return tableView
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ContributorsDetailsView {
    
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(tableView)
    }
    
    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
