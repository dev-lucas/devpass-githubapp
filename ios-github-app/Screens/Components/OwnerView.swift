//
//  OwnerView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 22/04/23.
//

import UIKit

struct OwnerViewConfiguration {
    var title: String
    var owner: String
    var bio: String
    var image: UIImage
    var html_url: String
}

final class OwnerView: UIView {
    
    private lazy var topBorder: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorModel.staticLightGray
        return view
    }()
    
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .bottom
        stackView.axis = .horizontal
        stackView.spacing = 10.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var buttonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView(image: UIImage())
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.layer.cornerRadius = 24
        return image
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var ownerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.textColor = ColorModel.title
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var bioLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = ColorModel.subtitle
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension OwnerView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(topBorder)
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(vStackView)
        vStackView.addArrangedSubview(infoStackView)
        vStackView.addArrangedSubview(imageView)
        infoStackView.addArrangedSubview(titleLabel)
        infoStackView.addArrangedSubview(ownerLabel)
        infoStackView.addArrangedSubview(bioLabel)
        mainStackView.addArrangedSubview(buttonView)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            topBorder.heightAnchor.constraint(equalToConstant: 0.5),
            topBorder.topAnchor.constraint(equalTo: topAnchor),
            topBorder.leadingAnchor.constraint(equalTo: leadingAnchor),
            topBorder.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 48),
            imageView.widthAnchor.constraint(equalToConstant: 48),
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14),
            buttonView.heightAnchor.constraint(equalToConstant: 56),
            heightAnchor.constraint(equalTo: mainStackView.heightAnchor, constant: 42),
        ])
    }
}

extension OwnerView {
    func updateView(with configuration: OwnerViewConfiguration) {
        titleLabel.text = configuration.title
        ownerLabel.text = configuration.owner
        bioLabel.text = configuration.bio
        imageView.image = configuration.image
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        
        buttonView.updateView(with: ButtonViewConfiguration(title: "See profile", action: {
            if let url = URL(string: configuration.html_url) {
                UIApplication.shared.open(url)
            }
        }))
    }
}
