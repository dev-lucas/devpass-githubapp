//
//  RepositoryTableViewCell.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 13/04/23.
//

import UIKit

struct RepositoryCellViewConfiguration {
    let repositoryName: String
    let repositoryOwner: String
}

final class RepositoryCellView: UITableViewCell {

    private lazy var repositoryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "repository-name"
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var repositoryOwnerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "git-user-name"
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.textColor = ColorModel.subtitle
        return label
    }()
    
    private lazy var imageAccessoryView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "chevron.right")
        imageView.tintColor = ColorModel.subtitle
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RepositoryCellView {
    func updateView(with configuration: RepositoryCellViewConfiguration) {
        repositoryNameLabel.text = configuration.repositoryName
        repositoryOwnerLabel.text = configuration.repositoryOwner
    }
}

private extension RepositoryCellView {
    func setupViews() {
        backgroundColor = ColorModel.background
        accessoryView = imageAccessoryView
        configureSubviews()
        configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(repositoryNameLabel)
        addSubview(repositoryOwnerLabel)
        addSubview(imageAccessoryView)
    }
    
    func configureSubviewsConstraints(){
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 70),
            repositoryNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 17),
            repositoryNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            repositoryNameLabel.trailingAnchor.constraint(equalTo: imageAccessoryView.leadingAnchor, constant: -16),
            repositoryOwnerLabel.topAnchor.constraint(equalTo: repositoryNameLabel.bottomAnchor),
            repositoryOwnerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            imageAccessoryView.heightAnchor.constraint(equalToConstant: 15),
            imageAccessoryView.widthAnchor.constraint(equalToConstant: 9),
            imageAccessoryView.topAnchor.constraint(equalTo: topAnchor, constant: 18),
            imageAccessoryView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
    }
}
