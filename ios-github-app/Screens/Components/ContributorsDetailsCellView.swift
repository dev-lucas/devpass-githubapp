//
//  ContributorsDetailsCellView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 01/05/23.
//

import UIKit

struct ContributorsDetailsCellViewConfiguration {
    let name: String
    let username: String
}

class ContributorsDetailsCellView: UITableViewCell {
    
    private lazy var auxiliaryView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorModel.background
        return view
    }()

    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.textColor = ColorModel.subtitle
        return label
    }()
    
    private(set) lazy var userImage: UIImageView = {
        let image = UIImageView(image: UIImage())
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.layer.cornerRadius = 24
        image.backgroundColor = ColorModel.button
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ContributorsDetailsCellView {
    func updateView(with configuration: ContributorsDetailsCellViewConfiguration) {
        nameLabel.text = configuration.name
        usernameLabel.text = String.ownerLogin(login: configuration.username)
        userImage.layer.masksToBounds = false
        userImage.clipsToBounds = true
    }
}

private extension ContributorsDetailsCellView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(userImage)
        mainStackView.addArrangedSubview(auxiliaryView)
        auxiliaryView.addSubview(infoStackView)
        infoStackView.addArrangedSubview(nameLabel)
        infoStackView.addArrangedSubview(usernameLabel)
    }
    
    func configureSubviewsConstraints(){
        NSLayoutConstraint.activate([
            userImage.heightAnchor.constraint(equalToConstant: 48),
            userImage.widthAnchor.constraint(equalToConstant: 48),
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14),
            infoStackView.leadingAnchor.constraint(equalTo: auxiliaryView.leadingAnchor),
            infoStackView.trailingAnchor.constraint(equalTo: auxiliaryView.trailingAnchor),
            infoStackView.centerYAnchor.constraint(equalTo: auxiliaryView.centerYAnchor)
            
        ])
    }
}
