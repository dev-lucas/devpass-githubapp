//
//  EmptyView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 17/04/23.
//

import UIKit

struct EmptyViewConfiguration {
    let title: String
    let subtitle: String
}

final class EmptyView: UIView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No repositories found"
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Search for users to see their public repositories here!"
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.textColor = ColorModel.subtitle
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension EmptyView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            subtitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 14),
            subtitleLabel.widthAnchor.constraint(equalToConstant: 283)
        ])
    }
}

extension EmptyView {
    func updateView(with configuration: EmptyViewConfiguration) {
        titleLabel.text = configuration.title
        subtitleLabel.text = configuration.subtitle
    }
}
