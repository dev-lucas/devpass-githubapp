//
//  CommitsDetailsCellView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 02/05/23.
//

import UIKit

struct CommitsDetailsCellViewConfiguration {
    let message: String
    let author: String
}

class CommitsDetailsCellView: UITableViewCell {
    
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.textColor = ColorModel.subtitle
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CommitsDetailsCellView {
    func updateView(with configuration: CommitsDetailsCellViewConfiguration) {
        messageLabel.text = String.replaceGitEmojiShortcodes(in: configuration.message)
        authorLabel.text = configuration.author
    }
}

private extension CommitsDetailsCellView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(messageLabel)
        mainStackView.addArrangedSubview(authorLabel)
    }
    
    func configureSubviewsConstraints(){
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        ])
    }
}
