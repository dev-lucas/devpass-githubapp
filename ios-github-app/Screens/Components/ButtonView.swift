//
//  ButtonView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 18/04/23.
//

import UIKit

struct ButtonViewConfiguration {
    let title: String
    var action: (() -> Void)? = nil
}

final class ButtonView: UIView {
    
    private lazy var button: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = .systemFont(ofSize: 17, weight: .semibold)
        button.tintColor = .white
        button.backgroundColor = ColorModel.button
        button.layer.cornerRadius = 14
        button.setTitle("ButtonView", for: .normal)
        return button
    }()

    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ButtonView {
    func setupViews() {
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(button)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor),
            button.bottomAnchor.constraint(equalTo: bottomAnchor),
            button.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
}

extension ButtonView {
    func updateView(with configuration: ButtonViewConfiguration) {
        button.setTitle(configuration.title, for: .normal)
        
        if let action = configuration.action {
            button.addAction((UIAction() { _ in action() }), for: .touchUpInside)
        }
    }
}
