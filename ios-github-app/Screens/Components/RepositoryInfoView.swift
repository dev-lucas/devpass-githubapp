//
//  RepositoryInfoView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 19/04/23.
//

import UIKit

struct RepositoryInfoViewConfiguration {
    let name: String
    let description: String
    let stars: Int
    let bifurcations: Int
}

final class RepositoryInfoView: UIView {
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var repositoryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private lazy var repositoryDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.textColor = ColorModel.title
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var repositoryInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = ColorModel.subtitle
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension RepositoryInfoView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(stackView)
        stackView.addArrangedSubview(repositoryNameLabel)
        stackView.addArrangedSubview(repositoryDescriptionLabel)
        stackView.addArrangedSubview(repositoryInfoLabel)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14),
            heightAnchor.constraint(equalTo: stackView.heightAnchor, constant: 42)
        ])
    }
}

extension RepositoryInfoView {
    func updateView(with configuration: RepositoryInfoViewConfiguration) {
        repositoryNameLabel.text = configuration.name
        repositoryDescriptionLabel.text = configuration.description
        repositoryInfoLabel.text = String.repositoryInfo(repositoriesCount: configuration.stars, bifurcationsCount: configuration.bifurcations)
    }
}
