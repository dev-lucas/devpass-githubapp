//
//  LanguagesView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 24/04/23.
//

import UIKit

struct LanguagesViewConfiguration {
    let title: String
    let mainLanguage: String
}

final class LanguagesView: UIView {

    private lazy var topBorder: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorModel.staticLightGray
        return view
    }()
    
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10.0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.textColor = ColorModel.title
        return label
    }()
    
    private(set) lazy var mainLanguageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.textColor = ColorModel.title
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    private(set) lazy var buttonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension LanguagesView {
    func setupViews() {
        backgroundColor = ColorModel.background
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(topBorder)
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(infoStackView)
        infoStackView.addArrangedSubview(titleLabel)
        infoStackView.addArrangedSubview(mainLanguageLabel)
        mainStackView.addArrangedSubview(buttonView)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            topBorder.heightAnchor.constraint(equalToConstant: 0.5),
            topBorder.topAnchor.constraint(equalTo: topAnchor),
            topBorder.leadingAnchor.constraint(equalTo: leadingAnchor),
            topBorder.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14),
            buttonView.heightAnchor.constraint(equalToConstant: 56),
            heightAnchor.constraint(equalTo: mainStackView.heightAnchor, constant: 42),
        ])
    }
}

extension LanguagesView {
    func updateView(with configuration: LanguagesViewConfiguration) {
        titleLabel.text = configuration.title
        mainLanguageLabel.text = String.languagesInfo(main: configuration.mainLanguage)
    }
}
