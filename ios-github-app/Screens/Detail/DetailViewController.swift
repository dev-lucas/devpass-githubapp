//
//  DetailViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

enum DetailType {
   case ownerInfo
   case ownerImage
   case repositoryCommits
   case repositoryContributors
   case languagesInfo
}

class DetailViewController: UIViewController {
    
    var repository: RepositoryModel?
    var owner: OwnerModel?
    var ownerImage: UIImage?
    var repositoryMainLanguage: String?
    var repositoryCommits: [CommitsModel]?
    var repositoryContributors: [OwnerModel]?
    var repositoryCommitsFullUrl: String?
    
    private let detailView: DetailView = {
        let detailView = DetailView()
        return detailView
    }()
    
    private let service = Service()
    
    override func viewDidLoad() {
        setupDetails()
    }
    
    override func loadView() {
        self.view = detailView
    }
    
    static func instance(_ repository: RepositoryModel) -> DetailViewController {
        let detailViewController: DetailViewController = DetailViewController(nibName: String(describing: self), bundle: nil)
        detailViewController.repository = repository
        return detailViewController
    }
}

private extension DetailViewController {
    
    func setupDetails() {
        setupNavigationBar()
        getInfo(for: repository?.owner.url, type: .ownerInfo)
        getInfo(for: repository?.owner.avatar_url, type: .ownerImage)
        getInfo(for: repository?.full_name, type: .repositoryCommits)
        getInfo(for: repository?.contributors_url, type: .repositoryContributors)
        setupRepositoryInfo()
        setupLicenseInfo()
        setupLanguagesInfo()
        validateViews()
    }
    
    func setupNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    func setupRepositoryInfo() {
        let repositoryInfoViewConfiguration = RepositoryInfoViewConfiguration(name: repository?.name ?? "-",
                                                                              description: repository?.description ?? "-",
                                                                              stars: repository?.stargazers_count ?? 0,
                                                                              bifurcations: repository?.forks_count ?? 0)
        
        detailView.repositoryInfoView.updateView(with: repositoryInfoViewConfiguration)
    }
    
    func setupOwnerInfo() {
        let configuration = OwnerViewConfiguration(title: "Owner",
                                                   owner: owner?.login ?? "-",
                                                   bio: owner?.bio ?? "-",
                                                   image: ownerImage ?? UIImage(),
                                                   html_url: owner?.html_url ?? "-")
        
        detailView.ownerView.updateView(with: configuration)
    }
    
    func setupLicenseInfo() {
        let configuration = LicenseViewConfiguration(title: "License",
                                                     name: repository?.license?.name ?? "-",
                                                     code: repository?.license?.spdx_id ?? "-",
                                                     url: repository?.license?.url ?? "-")
        
        detailView.licenseView.updateView(with: configuration)
    }
    
    func setupLanguagesInfo() {
        let configuration = LanguagesViewConfiguration(title: "Languages",
                                                       mainLanguage: repository?.language ?? "-")

        detailView.languagesView.updateView(with: configuration)
        detailView.languagesView.buttonView.updateView(with: ButtonViewConfiguration(title: "See languages", action: { [weak self] in
            self?.getInfo(for: self?.repository?.languages_url, type: .languagesInfo)
        }))
    }
       
    func setupCommitsInfo() {
        let configuration = CommitsViewConfiguration(title: "Commits",
                                                     count: repositoryCommits?.count ?? 0,
                                                     url: repositoryCommitsFullUrl ?? "-")

        detailView.commitsView.updateView(with: configuration)
        detailView.commitsView.buttonView.updateView(with: ButtonViewConfiguration(title: "See commits", action: { [weak self] in
            let controller = CommitsDetailsViewController.instance(self?.repositoryCommits ?? [])
            self?.navigationController?.pushViewController(controller, animated: true)
        }))
    }
    
    func setupContributorsInfo() {
        let configuration = ContributorsViewConfiguration(title: "Contributors",
                                                          count: repositoryContributors?.count ?? 0)
        
        detailView.contributorsView.updateView(with: configuration)
        detailView.contributorsView.buttonView.updateView(with: ButtonViewConfiguration(title: "See contributors", action: { [weak self] in
            let controller = ContributorsDetailsViewController.instance(self?.repositoryContributors ?? [])
            self?.navigationController?.pushViewController(controller, animated: true)
        }))
    }
}

extension DetailViewController {
    private func validateViews() {
        if detailView.licenseView.nameLabel.text == "-" {
            detailView.licenseView.removeFromSuperview()
        }
        
        if detailView.languagesView.mainLanguageLabel.text == String.languagesInfo(main: "-") {
            detailView.languagesView.removeFromSuperview()
        }
    }
}

extension DetailViewController {
    
    func getInfo(for url: String?, type: DetailType) {
        guard let url else { return }
    
        switch type {
        case .languagesInfo:
            service.get(for: url) { [weak self] (result: Result<LanguagesRepositoryModel, Error>) in
                self?.handleResponse(typeResponse: result) { response in
                    guard let response = response as? LanguagesRepositoryModel else { fatalError("Unable to parse response to LanguagesRepositoryModel") }
                    let controller = LanguagesDetailsViewController.instance(response)
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
            }
            break
            
        case .ownerImage:
            service.get(for: url) { [weak self] (result: Result<UIImage, Error>) in
                self?.handleResponse(typeResponse: result) { response in
                    self?.ownerImage = response as? UIImage
                    self?.setupOwnerInfo()
                }
            }
            break
            
        case .ownerInfo:
            service.get(for: url) { [weak self] (result: Result<OwnerModel, Error>) in
                self?.handleResponse(typeResponse: result) { response in
                    self?.owner = response as? OwnerModel
                    self?.setupOwnerInfo()
                }
            }
            break
            
        case .repositoryCommits:
            let fullUrl = "https://api.github.com/repos/\(url)/commits"
            service.get(for: fullUrl) { [weak self] (result: Result<[CommitsModel], Error>) in
                self?.handleResponse(typeResponse: result) { response in
                    self?.repositoryCommits = (response as? [CommitsModel])
                    self?.setupCommitsInfo()
                }
            }
            break
            
        case .repositoryContributors:
            service.get(for: url) { [weak self] (result: Result<[OwnerModel], Error>) in
                self?.handleResponse(typeResponse: result) { response in
                    self?.repositoryContributors = (response as? [OwnerModel])
                    self?.setupContributorsInfo()
                }
            }
            break
        }
    }
    
    private func handleResponse<T>(typeResponse: Result<T, Error>, successAction: @escaping (_ response: Any) -> Void) {
        DispatchQueue.main.async {
            switch typeResponse {
            case .success(let success):
                successAction(success)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
