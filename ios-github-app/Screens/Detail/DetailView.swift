//
//  DetailView.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

class DetailView: UIView {
        
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.backgroundColor = ColorModel.background
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.layoutMargins = .zero
        return scrollView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.backgroundColor = ColorModel.background
        return stackView
    }()
    
    lazy var repositoryInfoView: RepositoryInfoView = {
        let view = RepositoryInfoView()
        return view
    }()
    
    lazy var ownerView: OwnerView = {
        let view = OwnerView()
        return view
    }()
    
    lazy var licenseView: LicenseView = {
        let view = LicenseView()
        return view
    }()
    
    lazy var languagesView: LanguagesView = {
        let view = LanguagesView()
        return view
    }()
    
    lazy var commitsView: CommitsView = {
        let view = CommitsView()
        return view
    }()
    
    lazy var contributorsView: ContributorsView = {
        let view = ContributorsView()
        return view
    }()
    
    init() {
        super.init(frame: .zero)
        self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension DetailView {
    func setupViews() {
        self.backgroundColor = ColorModel.background
        self.configureSubviews()
        self.configureSubviewsConstraints()
    }
    
    func configureSubviews() {
        addSubview(scrollView)
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(repositoryInfoView)
        stackView.addArrangedSubview(ownerView)
        stackView.addArrangedSubview(licenseView)
        stackView.addArrangedSubview(languagesView)
        stackView.addArrangedSubview(commitsView)
        stackView.addArrangedSubview(contributorsView)
    }
    
    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
    }
}
