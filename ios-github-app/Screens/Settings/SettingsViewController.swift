//
//  SettingsViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

final class SettingsViewController: UIViewController {

    private let listVersions: [String] = ["Version 1.0"]
    
    private let settingsView: SettingsView = {
        let settingsView = SettingsView()
        return settingsView
    }()
    
    override func viewDidLoad() {
        setupNavigationBar()
        setupTableView()
    }
    
    override func loadView() {
        self.view = settingsView
    }
    
    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Settings"
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        settingsView.tableView.dataSource = self
    }
}

extension SettingsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listVersions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: settingsView.settingsViewCellIdentifier) else { fatalError("Generate SettingsViewCell error") }
        cell.textLabel?.text = listVersions[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "APP VERSION"
    }
}
