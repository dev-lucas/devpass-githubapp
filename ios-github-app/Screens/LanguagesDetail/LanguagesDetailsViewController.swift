//
//  LanguagesDetailsViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 25/04/23.
//

import UIKit

final class LanguagesDetailsViewController: UIViewController {
    
    private var languages: LanguagesRepositoryModel?
    
    private let languagesDetailsView: LanguagesDetailsView = {
        let view = LanguagesDetailsView()
        return view
    }()
    
    override func viewDidLoad() {
        setupNavigationBar()
        setupTableView()
    }
    
    override func loadView() {
        self.view = languagesDetailsView
    }
    
    static func instance(_ languages: LanguagesRepositoryModel) -> LanguagesDetailsViewController {
        let controller: LanguagesDetailsViewController = LanguagesDetailsViewController(nibName: String(describing: self), bundle: nil)
        controller.languages = languages
        return controller
    }
    
    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Languages"
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        languagesDetailsView.tableView.dataSource = self
        languagesDetailsView.tableView.delegate = self
    }
}

extension LanguagesDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        languages?.parsedLanguages.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LanguagesDetailsCellView.classIdentifier()) as? LanguagesDetailsCellView else { fatalError("Generate LanguagesDetailsCellView error") }
        guard let language = languages?.parsedLanguages[indexPath.row] else { fatalError("Language Cell info error") }
        cell.separatorInset.left = 16
        cell.selectionStyle = .none
        cell.updateView(with: LanguagesDetailsCellViewConfiguration(name: language.name, code: language.code))
        return cell
    }
}

extension LanguagesDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 90 : 70
    }
}
