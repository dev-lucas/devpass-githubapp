//
//  ListViewController.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

final class ListViewController: UIViewController {

    private var listItens: [RepositoryModel] = []
    
    private let listView: ListView = {
        let listView = ListView()
        return listView
    }()
    
    private let emptyView: EmptyView = {
        let emptyView = EmptyView()
        return emptyView
    }()
    
    private let loadingView: LoadingView = {
        let loadingView = LoadingView()
        return loadingView
    }()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController()
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Type a GitHub user name"
        return searchController
    }()

    private let service = Service()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        setupNavigationBar()
        setupTableView()
        getRepositories()
    }

    override func viewDidAppear(_ animated: Bool) {
    }

    override func loadView() {
        view = listView
    }
    
    private func getRepositories(for user: String = "devpass-tech") {
        let url = "https://api.github.com/users/\(user)/repos"
        view = loadingView
        loadingView.startAnimating()
        service.get(for: url) { [weak self] (response: Result<[RepositoryModel], Error>) in
            DispatchQueue.main.async {
                switch response {
                case .success(let repositories):
                    self?.listItens = repositories
                    if repositories.isEmpty {
                        self?.view = self?.emptyView
                    } else {
                        self?.view = self?.listView
                        self?.listView.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    print(error)
                    self?.view = self?.emptyView
                    break
                }
            }
        }
    }
    
    private func setupNavigationBar() {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = ColorModel.secondaryBackground
        
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Repositories"
        
        let settingsButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settingsButton))
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: .none)
        settingsButton.tintColor = ColorModel.button
        backButton.tintColor = ColorModel.button
        
        navigationItem.rightBarButtonItem = settingsButton
        navigationItem.backBarButtonItem = backButton
        navigationItem.searchController = searchController
    }
    
    private func setupTableView() {
        listView.tableView.delegate = self
        listView.tableView.dataSource = self
        listView.tableView.reloadData()
    }
    
    @objc private func settingsButton(sender: UIButton) {
        let settingsViewController = UINavigationController(rootViewController: SettingsViewController(nibName: "SettingsViewController", bundle: nil))
        present(settingsViewController, animated: true)
    }
}

extension ListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        searchController.isActive = false
        searchBar.text = text
        getRepositories(for: text)
    }
}

extension ListViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItens.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryCellView.classIdentifier()) as? RepositoryCellView else { fatalError("Generate cell error") }
        let repositoryInfo = listItens[indexPath.row]
        cell.separatorInset.left = 16
        cell.updateView(with: RepositoryCellViewConfiguration(repositoryName: repositoryInfo.name, repositoryOwner: repositoryInfo.owner.login))
        return cell
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = DetailViewController.instance(listItens[indexPath.row])
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}
