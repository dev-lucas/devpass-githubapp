//
//  String+Extensions.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import Foundation

extension String {

    static func repositoryInfo(repositoriesCount: Int, bifurcationsCount: Int) -> String {
        return "\(repositoriesCount) stars   \(bifurcationsCount) bifurcations"
    }
    
    static func languagesInfo(main language: String) -> String {
        return "Main language: \(language)"
    }
    
    static func commitsInfo(count: Int) -> String {
        return "\(count) commit\(pluralValidate(count))"
    }
    
    static func contributorsInfo(count: Int) -> String {
        return "\(count) contributor\(pluralValidate(count))"
    }

    static func ownerLogin(login: String) -> String {
        return "@\(login)"
    }

    static func replaceGitEmojiShortcodes(in fullMessage: String) -> String {
        let emojiShortcodes = [
            ":sparkles:": "✨",
            ":bug:": "🐛",
            ":ambulance:": "🚑",
            ":lipstick:": "💄",
            ":tada:": "🎉",
            ":fire:": "🔥",
            ":art:": "🎨",
            ":sparkler:": "🎇",
            ":memo:": "📝",
            ":rocket:": "🚀",
            ":pencil2:": "✏️",
            ":recycle:": "♻️",
            ":white_check_mark:": "✅",
            ":lock:": "🔒",
            ":arrow_up:": "⬆️",
            ":zap:": "⚡️",
            ":arrow_down:": "⬇️",
            ":construction:": "🚧",
            ":wrench:": "🔧",
            ":artificial_satellite:": "🛰️",
            ":construction_worker:": "👷",
            ":building_construction:": "🏗️",
            ":gem:": "💎",
            ":sparkling_heart:": "💖",
            ":truck:": "🚚",
            ":package:": "📦",
            ":bookmark:": "🔖",
            ":fireworks:": "🎆",
            ":bento:": "🍱",
            ":heavy_check_mark:": "✔️",
            ":wastebasket:": "🗑️"
        ]
        var replacedText = fullMessage
        
        for (shortcode, emoji) in emojiShortcodes {
            replacedText = replacedText.replacingOccurrences(of: shortcode, with: emoji)
        }
        
        return replacedText
    }
    
    private static func pluralValidate(_ count: Int) -> String {
        return (count == 1) ? "" : "s"
    }
}
