//
//  UIViewController+Extensions.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

extension UIViewController {

    func insideNavigationController() -> UINavigationController {

        let navigationController = UINavigationController(rootViewController: self)
        navigationController.modalPresentationStyle = .formSheet

        return navigationController
    }
}
