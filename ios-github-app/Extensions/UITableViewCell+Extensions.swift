//
//  UITableViewCell+Extensions.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

extension UITableViewCell {

    class func classIdentifier() -> String {
        guard let className = String(describing: self).components(separatedBy: ".").last else {
            return ""
        }

        return className
    }
}
