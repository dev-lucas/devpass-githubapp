//
//  NetworkManagerService.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 17/04/23.
//

import UIKit

enum NetworkError: Error {
    case invalidResponse
    case emptyData
    case invalidImage
    case cachedResponse
}

// A network manager that can perform HTTP requests and decode the response into a generic type T.
protocol NetworkManagerServiceProtocol {
    func get<T: Decodable>(request: URLRequest, completion: @escaping (Result<T, Error>) -> Void)
    func get(request: URLRequest, completion: @escaping (Result<UIImage, Error>) -> Void)
}

final class NetworkManagerService: NetworkManagerServiceProtocol {

    private let session: URLSession
    private var dataTask: URLSessionDataTask?

    init(session: URLSession = .shared) {
        self.session = session
    }
    
    deinit {
        dataTask?.cancel()
    }

    func get<T: Decodable>(request: URLRequest, completion: @escaping (Result<T, Error>) -> Void) {
        dataTask = session.dataTask(with: request) { data, response, error in
            self.handleResponse(response: response, data: data, error: error, completion: completion)
        }
        
        dataTask?.resume()
    }
    
    func get(request: URLRequest, completion: @escaping (Result<UIImage, Error>) -> Void) {
        let memoryCapacityInMb = 10
        let diskCapacityInMb = 50

        if let cachedResponse = URLCache.shared.cachedResponse(for: request),
           let image = UIImage(data: cachedResponse.data) {
            completion(.success(image))
            return
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = URLCache(memoryCapacity: memoryCapacityInMb * 1024 * 1024, diskCapacity: diskCapacityInMb * 1024 * 1024, diskPath: "myImageCache")
        let session = URLSession(configuration: configuration)
        
        dataTask = session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }
            
            guard let data = data else {
                completion(.failure(NetworkError.emptyData))
                return
            }
            
            guard let image = UIImage(data: data) else {
                completion(.failure(NetworkError.invalidImage))
                return
            }

            guard let response = response else {
                completion(.failure(NetworkError.cachedResponse))
                return
            }
            
            let cachedResponse = CachedURLResponse(response: response, data: data)
            URLCache.shared.storeCachedResponse(cachedResponse, for: request)
            
            completion(.success(image))
        }
        
        dataTask?.resume()
    }
    
    private func handleResponse<T: Decodable>(response: URLResponse?, data: Data?, error: Error?, completion: @escaping (Result<T, Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
            completion(.failure(NetworkError.invalidResponse))
            return
        }
        
        guard let data = data else {
            completion(.failure(NetworkError.emptyData))
            return
        }
        
        do {
            let decodedData = try JSONDecoder().decode(T.self, from: data)
            completion(.success(decodedData))
        } catch {
            completion(.failure(error))
        }
    }
}
