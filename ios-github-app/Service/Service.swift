//
//  ListService.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

struct Service {
    
    private let networkManager: NetworkManagerServiceProtocol
    
    init(networkManager: NetworkManagerServiceProtocol = NetworkManagerService()) {
        self.networkManager = networkManager
    }
    
    func get<T: Decodable>(for url: String, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
            return
        }
        
        let request = URLRequest(url: url)
        networkManager.get(request: request, completion: completion)
    }
    
    func get(for url: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
            return
        }
        
        let request = URLRequest(url: url)
        networkManager.get(request: request, completion: completion)
    }
    
    func get<T: Decodable>(for url: String, completion: @escaping (Result<[T], Error>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
            return
        }
        
        let request = URLRequest(url: url)
        networkManager.get(request: request, completion: completion)
    }
}
