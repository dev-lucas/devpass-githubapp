//
//  SceneDelegate.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(rootViewController: ListViewController())
        self.window?.windowScene = windowScene
        self.window?.makeKeyAndVisible()
    }
}
