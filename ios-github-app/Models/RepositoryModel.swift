//
//  RepositoryModel.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 12/04/23.
//

import Foundation

struct RepositoryModel: Decodable {
    let name: String
    let description: String?
    let owner: OwnerModel
    let stargazers_count: Int?
    let forks_count: Int?
    let license: LicenseModel?
    let languages_url: String
    let full_name: String
    let contributors_url: String
    let language: String?
}
