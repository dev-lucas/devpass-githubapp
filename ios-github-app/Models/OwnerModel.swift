//
//  OwnerModel.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 19/04/23.
//

import Foundation

struct OwnerModel: Decodable {
    let login: String
    let url: String
    let avatar_url: String?
    let bio: String?
    let html_url: String
    let name: String?
}
