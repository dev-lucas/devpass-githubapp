//
//  LicenseModel.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 23/04/23.
//

import Foundation

struct LicenseModel: Decodable {
    let name: String
    let spdx_id: String
    let url: String?
}
