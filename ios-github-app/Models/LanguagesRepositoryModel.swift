//
//  LanguagesModel.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 24/04/23.
//

import Foundation

struct LanguagesModel {
    let name: String
    let code: Int
}

struct LanguagesRepositoryModel: Decodable {
   
    var parsedLanguages: [LanguagesModel] = []
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let dict = try container.decode([String:Int].self)

        for key in dict.keys {
            parsedLanguages.append(LanguagesModel(name: key, code: dict[key] ?? 0))
        }
    }
}
