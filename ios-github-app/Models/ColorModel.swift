//
//  ColorService.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 24/04/23.
//

import UIKit

struct ColorModel {
    static let button = UIColor.systemBlue
    static let title = UIColor.label
    static let secondaryBackground = UIColor.secondarySystemBackground
    static let subtitle = UIColor.secondaryLabel
    static let background = UIColor.systemBackground
    static let staticLightGray = UIColor.lightGray
}
