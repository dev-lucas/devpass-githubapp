//
//  CommitsModel.swift
//  ios-github-app
//
//  Created by Lucas Gomes on 24/04/23.
//

import Foundation

struct CommitDetailModel: Decodable {
    let message: String
    let author: CommitDetailAuthorModel
}

struct CommitDetailAuthorModel: Decodable {
    let name: String
}

struct CommitAuthorModel: Decodable {
    let login: String
}

struct CommitsModel: Decodable {
    let commit: CommitDetailModel?
    let author: CommitAuthorModel?
}
